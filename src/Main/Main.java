package Main;

import Animals.*;

public class Main {

    public static void main(String[] args) {

        Dog dog = new Dog("Sharik");
        Wolf wolf = new Wolf("Wolf");
        Bear bearWilly = new Bear("Willy");

        // Example 1: dog
        {
            dog.setEating(true);
            dog.printState();
            System.out.println(dog + " voice: " + dog.getVoice() + "\n");

            dog.setSleeping(true);
            dog.printState();
            System.out.println(dog + " voice: " + dog.getVoice() + "\n");

            dog.setEating(true);
            dog.printState();
            System.out.println(dog + " voice: " + dog.getVoice() + "\n");
        }

        System.out.println("--------------------------------------------");

        // Example 2: wolf
        {
            wolf.setSleeping(true);
            wolf.printState();
            System.out.println(wolf + " voice: " + wolf.getVoice() + "\n");

            wolf.setSleeping(false);
            wolf.printState();
            System.out.println(wolf + " voice: " + wolf.getVoice() + "\n");

            wolf.setEating(true);
            wolf.printState();
            System.out.println(wolf + " voice: " + wolf.getVoice() + "\n");
        }

        System.out.println("--------------------------------------------");

        // Example 3: bearWilly
        {
            bearWilly.printState();
            System.out.println(bearWilly + " voice: " + bearWilly.getVoice() + "\n");

            bearWilly.setEating(true);
            bearWilly.printState();
            System.out.println(bearWilly + " voice: " + bearWilly.getVoice() + "\n");

            bearWilly.setSleeping(true);
            bearWilly.printState();
            System.out.println(bearWilly + " voice: " + bearWilly.getVoice() + "\n");
        }


    }
}
