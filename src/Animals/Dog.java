package Animals;

public class Dog extends Animal{

    public Dog(String name) {
        super(name);
    }

    @Override
    public String voice() {
        return "Bow-wow";
    }
}
