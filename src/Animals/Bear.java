package Animals;

public class Bear extends Animal{

    public Bear(String name) {
        super(name);
    }

    @Override
    public String voice() {
        return "Growl";
    }
}
