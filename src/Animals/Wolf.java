package Animals;

public class Wolf extends Animal{

    public Wolf(String name) {
        super(name);
    }

    @Override
    public String voice() {
        return "Howl";
    }
}
