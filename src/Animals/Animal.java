package Animals;


public abstract class Animal{

    protected String name;
    protected Boolean sleeping;
    protected Boolean eating;

    public Animal(){
        this("");
    }

    public Animal(String name){
        this.name = name;
        this.sleeping = false;
        this.eating = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isSleeping(){
        return this.sleeping;
    }

    public Boolean isEating(){
        return this.eating;
    }

    public void setSleeping(Boolean sleeping) {
        if (sleeping){
            setEating(false);
        }

        this.sleeping = sleeping;
    }

    public void setEating(Boolean eating) {
        if (eating){
            setSleeping(false);
        }

        this.eating = eating;
    }

    /**
     * Функция возвращает строку описывающую звук издаваемый животным
     * * @return String
     */
    public abstract String voice();

    public void printState() {
        String sleepingStatus   = isSleeping()  ? "sleeping"   : "not sleeping";
        String eatingStatus     = isEating()    ? "eating"     : "not eating";

        System.out.printf("%s %s and %s%n", this, sleepingStatus, eatingStatus);
    }

    public String getVoice() {
        String voice = "";
        if (!isSleeping()){
            voice = voice();
        }

        return voice;
    }

    @Override
    public String toString(){
        return name;
    }
}
